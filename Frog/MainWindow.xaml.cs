﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Frog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private StartGameControl startGameControl;
        public GameControl gameControl;
        public PlayGame playGame;
        public GameState gameState = new GameState();


        public MainWindow()
        {
            //loading the XAML UI content at XAML parse time
            InitializeComponent();

            this.startGameControl = new StartGameControl(this);
            this.gameControl = new GameControl(this);
            this.playGame = new PlayGame(this);            

            this.Content = startGameControl;
        }

        //Method switches MainWindow's content to StartGameControl
        public void SwitchToGame()
        {
            this.Content = gameControl;
        }

        public void SwitchToStartGame()
        {
            this.Content = startGameControl;
        }
                       
        //Save the GameState (Serialization)
        public void SaveGame()
        {
            using (FileStream fileStream = new FileStream(@"save.froggygame", FileMode.Create))
            {
                BinaryFormatter binaryFormater = new BinaryFormatter();
                binaryFormater.Serialize(fileStream, this.gameState);
            }
        }

        //Load the GameState (Deserialization)
        public void LoadGame()
        {
            try
            {
                using (FileStream fileStream = new FileStream(@"save.froggygame", FileMode.Open))
                {
                    BinaryFormatter binaryFormater = new BinaryFormatter();
                    this.gameState = (GameState)binaryFormater.Deserialize(fileStream);
                }
            }
            catch (Exception ex)
            {
                //if it can not deserialize PlayGame it will show the message
                MessageBox.Show(ex.Message);
                return;
            }

            SwitchToGame();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (playGame.Alive == false)
                return;
            if (gameState.End == true)
                return;

            switch (e.Key)
            {
                case Key.Down:
                    {
                        double moveTop = Canvas.GetTop(playGame.images[PlayGame.indexOfFrog]);
                        if (moveTop >= 480)
                            break;
                        Canvas.SetTop(playGame.images[PlayGame.indexOfFrog], moveTop += playGame.images[PlayGame.indexOfFrog].Width);
                        break;
                    }
                case Key.Left:
                    {
                        double moveLeft = Canvas.GetLeft(playGame.images[PlayGame.indexOfFrog]);
                        if (moveLeft <= 0)
                            break;
                        Canvas.SetLeft(playGame.images[PlayGame.indexOfFrog], moveLeft -= playGame.images[PlayGame.indexOfFrog].Width);
                        break;
                    }
                case Key.Right:
                    {
                        double moveLeft = Canvas.GetLeft(playGame.images[PlayGame.indexOfFrog]);
                        if (moveLeft >= 360)
                            break;
                        Canvas.SetLeft(playGame.images[PlayGame.indexOfFrog], moveLeft += playGame.images[PlayGame.indexOfFrog].Width);
                        break;
                    }
                case Key.Up:
                    {
                        double moveTop = Canvas.GetTop(playGame.images[PlayGame.indexOfFrog]);
                        if (moveTop <= 0)
                            break;
                        Canvas.SetTop(playGame.images[PlayGame.indexOfFrog], moveTop -= playGame.images[PlayGame.indexOfFrog].Width);
                        break;
                    }
                default:
                        break;
                    
            }
        }
    }
}
