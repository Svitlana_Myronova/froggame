﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Frog
{
    /// <summary>
    /// Interaction logic for StartGameControl.xaml
    /// </summary>
    public partial class StartGameControl : UserControl
    {
        private MainWindow mainWindow;
       // private PlayGame playGame;

        public StartGameControl(MainWindow mainWindow)
        {
            //loading the XAML UI content at XAML parse time
            InitializeComponent();
            this.mainWindow = mainWindow;
           //this.playGame = new PlayGame(this.mainWindow);
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            this.mainWindow.SwitchToGame();
            this.mainWindow.playGame.StartOfGame();
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            this.mainWindow.Close();
        }

        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {
            this.mainWindow.LoadGame();
            this.mainWindow.playGame.LoadOfGame();
        }
    }
}
