﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frog
{
    [Serializable]
    public class GameState
    {
        public double[] movesLeft = new double[21];
        public double[] movesTop = new double[21];

        public bool End { set; get; } = false;

        public int Score { set; get; } = 0;

        public int SwitchNumber { set; get; } = 0;

        public double CrocodileImgLifetime { set; get; } = 0;
    }
}
