﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Frog
{
    [Serializable]
    public class PlayGame
    {
        private MainWindow mainWindow;

        public Image[] images = new Image[21];

        Image[] imagesPlaces = new Image[4]; 

        public static int indexOfFrog = 20;

        public bool Alive
        {
            private set;
            get;
        } = true;
       
        static double oneTickSec = 0.01;
        static double FailImgLifetime = 2000 * oneTickSec;
        

        public DispatcherTimer gameTimer = new DispatcherTimer();

        Random rand = new Random();

        BitmapImage Place = new BitmapImage(new Uri(@"pack://application:,,,/Images/place.png"));
        BitmapImage Crocodile = new BitmapImage(new Uri(@"pack://application:,,,/Images/crocodile.png"));

        public PlayGame(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;

            Initialize();

            //setup timers for all game with interval    oneTickSec   sec
            gameTimer.Interval = TimeSpan.FromSeconds(oneTickSec);
            //event after each 0.01 sec
            //cteates arrayList of EventHendlers. But in this game we need only one EventHandler
            gameTimer.Tick += new EventHandler(GameTimer_Tick);

            this.mainWindow.gameState.CrocodileImgLifetime = 4000 * oneTickSec;

        }

        private void Initialize()
        {
            images[0] = mainWindow.gameControl.img_0;
            images[1] = mainWindow.gameControl.img_1;
            images[2] = mainWindow.gameControl.img_2;
            images[3] = mainWindow.gameControl.img_3;
            images[4] = mainWindow.gameControl.img_4;
            images[5] = mainWindow.gameControl.img_5;
            images[6] = mainWindow.gameControl.img_6;
            images[7] = mainWindow.gameControl.img_7;
            images[8] = mainWindow.gameControl.img_8;
            images[9] = mainWindow.gameControl.img_9;
            images[10] = mainWindow.gameControl.img_10;
            images[11] = mainWindow.gameControl.img_11;
            images[12] = mainWindow.gameControl.img_12;
            images[13] = mainWindow.gameControl.img_13;
            images[14] = mainWindow.gameControl.img_14;
            images[15] = mainWindow.gameControl.img_15;
            images[16] = mainWindow.gameControl.img_16;
            images[17] = mainWindow.gameControl.img_17;
            images[18] = mainWindow.gameControl.img_18;
            images[19] = mainWindow.gameControl.img_19;

            images[indexOfFrog] = mainWindow.gameControl.img_20;
            images[indexOfFrog].Source = new BitmapImage(new Uri(@"pack://application:,,,/Images/froggy.png"));

            for (int i = 0; i< images.Length-1; i++)
            {                
                if (i > 9)
                    images[i].Source = new BitmapImage(new Uri(@"pack://application:,,,/Images/" + (i - 10 + 1) + ".png"));
                else
                    images[i].Source = new BitmapImage(new Uri(@"pack://application:,,,/Images/" + (i + 1) + ".png"));
            }

            imagesPlaces[0] = mainWindow.gameControl.img_place_0;
            imagesPlaces[1] = mainWindow.gameControl.img_place_1;
            imagesPlaces[2] = mainWindow.gameControl.img_place_2;
            imagesPlaces[3] = mainWindow.gameControl.img_place_3;

            for (int i = 0; i < imagesPlaces.Length; i++)
            {
                imagesPlaces[i].Source = Place;
            }

        }


        private void GameTimer_Tick(object sender, EventArgs e)
        {
            mainWindow.gameState.Score++;
            for (int i = 0; i < mainWindow.gameState.movesLeft.Length-1; i++)
            {
                mainWindow.gameState.movesLeft[i] = Canvas.GetLeft(images[i]);
                
                if (mainWindow.gameState.movesLeft[i] < -images[i].Width)
                    mainWindow.gameState.movesLeft[i] = 400;
                if (mainWindow.gameState.movesLeft[i] > 400 + images[i].Width)
                    mainWindow.gameState.movesLeft[i] = -images[i].Width;

                if (i%2 != 0)
                {
                    Canvas.SetLeft(images[i], mainWindow.gameState.movesLeft[i] - 1);
                }
                else
                {
                    Canvas.SetLeft(images[i], mainWindow.gameState.movesLeft[i] + 1);
                }                
            }

            ChangeToCrocodile();
            GameLogicWithCars();
            GameLogicWithLogs();
            IsFroggyAlive();
        }
        
        public void StartOfGame()
        {
            //frog's position
            Canvas.SetLeft(images[indexOfFrog], 160);
            Canvas.SetTop(images[indexOfFrog], 480);
            mainWindow.gameState.End = false;
            //start timer
            gameTimer.Start();
        }

        public void LoadOfGame()
        {
            //frog's position
            for (int i=0; i < mainWindow.gameState.movesLeft.Length; i++ )
            {
                Canvas.SetLeft(images[i], mainWindow.gameState.movesLeft[i]);
                Canvas.SetTop(images[i], mainWindow.gameState.movesTop[i]);
            }

            for (int i=0; i < imagesPlaces.Length; i++)
            {
                if (i == mainWindow.gameState.SwitchNumber)
                {
                    imagesPlaces[i].Source = Crocodile;
                }
                else
                {
                    imagesPlaces[i].Source = Place;
                }
            }

            //start timer
            gameTimer.Start();
        }

        private void GameLogicWithCars()
        {
            for (int i = 0; i < mainWindow.gameState.movesTop.Length; i++)
            {
                mainWindow.gameState.movesTop[i] = Canvas.GetTop(images[i]);
                mainWindow.gameState.movesLeft[i] = Canvas.GetLeft(images[i]);
            }
            if (mainWindow.gameState.movesTop[indexOfFrog] > 240 && mainWindow.gameState.movesTop[indexOfFrog] < 480)
            {
                //this logic only for cars, not for logs
                for (int i = 0; i < mainWindow.gameState.movesTop.Length-1; i++)  
                {
                    if ((mainWindow.gameState.movesTop[indexOfFrog] > (mainWindow.gameState.movesTop[i] - images[indexOfFrog].Height) && 
                        mainWindow.gameState.movesTop[indexOfFrog] < (mainWindow.gameState.movesTop[i] + images[i].Height)) &&
                        ((mainWindow.gameState.movesLeft[indexOfFrog] + images[indexOfFrog].Width / 2) >= mainWindow.gameState.movesLeft[i] && 
                        mainWindow.gameState.movesLeft[indexOfFrog] <= (mainWindow.gameState.movesLeft[i] + images[i].Width / 1.5)))
                    {
                        Alive = false;

                    }
                }
            }
        }

        private void GameLogicWithLogs() 
        {
            for (int i = 0; i < mainWindow.gameState.movesTop.Length; i++)
            {
                mainWindow.gameState.movesTop[i] = Canvas.GetTop(images[i]);
                mainWindow.gameState.movesLeft[i] = Canvas.GetLeft(images[i]);
            }
            //if frog became dead but the log will approach it, the frog will be alive. So
            if (Alive == false)
                return;

            if (mainWindow.gameState.movesTop[indexOfFrog] == 0 &&  
                                              ((mainWindow.gameState.movesLeft[indexOfFrog] -48 > -4 && mainWindow.gameState.movesLeft[indexOfFrog] - 48 < 4 && imagesPlaces[0].Source == Place) || 
                                               (mainWindow.gameState.movesLeft[indexOfFrog] -136 > -4 && mainWindow.gameState.movesLeft[indexOfFrog] - 136 < 4 && imagesPlaces[1].Source == Place) || 
                                               (mainWindow.gameState.movesLeft[indexOfFrog] -224 > -4 && mainWindow.gameState.movesLeft[indexOfFrog] - 224 < 4 && imagesPlaces[2].Source == Place) || 
                                               (mainWindow.gameState.movesLeft[indexOfFrog] -312 > -4 && mainWindow.gameState.movesLeft[indexOfFrog] - 312 < 4 && imagesPlaces[3].Source == Place)))
            {
                EndOfGame();
                return;
            }

            if (mainWindow.gameState.movesTop[indexOfFrog] >= 0 && mainWindow.gameState.movesTop[indexOfFrog] < 240)
            {
                Alive = false;
                //this logic only for logs
                for (int i = 0; i < mainWindow.gameState.movesTop.Length - 1; i++)
                {
                    if (  ((mainWindow.gameState.movesTop[indexOfFrog] + images[indexOfFrog].Height / 2) - (mainWindow.gameState.movesTop[i] + images[i].Height / 2) > -1 && 
                        (mainWindow.gameState.movesTop[indexOfFrog] + images[indexOfFrog].Height / 2) - (mainWindow.gameState.movesTop[i] + images[i].Height / 2) < 1) &&
                        (mainWindow.gameState.movesLeft[indexOfFrog] > mainWindow.gameState.movesLeft[i] && mainWindow.gameState.movesLeft[indexOfFrog] < (mainWindow.gameState.movesLeft[i] + images[i].Width)))
                    {
                        Alive = true;

                        if (i % 2 != 0)
                        {
                            Canvas.SetLeft(images[indexOfFrog], mainWindow.gameState.movesLeft[indexOfFrog] - 1);
                            if (mainWindow.gameState.movesLeft[indexOfFrog] <= 0)
                                Alive = false;
                        }
                        else
                        {
                            Canvas.SetLeft(images[indexOfFrog], mainWindow.gameState.movesLeft[indexOfFrog] + 1);
                            if (mainWindow.gameState.movesLeft[indexOfFrog] >= 360)
                                Alive = false;
                        }
                    }                        
                }
            }
        }

        private void EndOfGame()
        {
            mainWindow.gameState.End = true;
            gameTimer.Stop();
            MessageBox.Show("YOU WIN! YOUR SCORE IS " + mainWindow.gameState.Score);
            mainWindow.SwitchToStartGame();

        }            

        private void IsFroggyAlive()
        {
            
            if (Alive == false)
            {
                images[indexOfFrog].Source = new BitmapImage(new Uri(@"pack://application:,,,/Images/fail.png"));
                Canvas.SetZIndex(images[indexOfFrog], 3);
                if (FailImgLifetime == 0)
                {
                    Alive = true;
                    Canvas.SetTop(images[indexOfFrog], 480);
                    Canvas.SetLeft(images[indexOfFrog], 160);
                }
                FailImgLifetime--;
            }
            if (Alive == true)
            {
                Canvas.SetZIndex(images[indexOfFrog], 2);               
                images[indexOfFrog].Source = new BitmapImage(new Uri(@"pack://application:,,,/Images/froggy.png"));
                FailImgLifetime = 2000 * oneTickSec;
            }
            return;

        }

        private void ChangeToCrocodile()
        {
            mainWindow.gameState.CrocodileImgLifetime--;

            if (mainWindow.gameState.CrocodileImgLifetime == 0)
            {
                imagesPlaces[mainWindow.gameState.SwitchNumber].Source = Place;
                mainWindow.gameState.SwitchNumber = rand.Next(4);
                imagesPlaces[mainWindow.gameState.SwitchNumber].Source = Crocodile;
                mainWindow.gameState.CrocodileImgLifetime = 4000 * oneTickSec;
            }            
        }
        
    }
}
