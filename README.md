# Frog Game

The game is written in C# using Windows Presentation Foundation framework. The idea of ​​the game is to help the frog to get to the black squares that are on the opposite side of the board. The player can move the frog with the arrow keys <kbd>↑</kbd>, <kbd>↓</kbd>, <kbd>←</kbd> and <kbd>→</kbd>. The frog needs to cross the road and not get hit by any car. It must jump on floating logs and lilies and not fall into the water. The frog should land on a black square where there is no crocodile. If the frog fails to avoid a collision with a car, falls into the water or is eaten by a crocodile, it returns to the starting position.

![Screenshot](Capture_2.png) &nbsp;&nbsp; ![Screenshot](Capture_3.png) &nbsp;&nbsp; ![Screenshot](Capture_4.png)

The game also has a function to save the current position of the frog and a menu: 

![Screenshot](Capture_1.png)

-   **Start** - starting the new game.
-	**Load** - loading the priveous one.
-	**Exit** - exiting the current game.
 
After the successful completion of the game, points are displayed.

![Screenshot](Capture_5.png) 